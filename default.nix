let
  pkgs = import (import ./nix/sources.nix).nixpkgs {};

  configuration = import ./configuration.nix;

  makeSystem = extraModules: let
    node = import (pkgs.path + /nixos) {
      system = "x86_64-linux";
      configuration = configuration extraModules;
    };
  in node;

  testingModule = {config, ...}: {
    boot.loader.grub.device = "/dev/sda";
    fileSystems."/" =
      {
        device = "/dev/sda";
        fsType = "ext4";
      };

    users.extraUsers.root.password = "";
    virtualisation = {
      diskSize = 8 * 1024;
      memorySize = 2 * 1024;
    };
  };

  

in {
  system = (makeSystem []).system;
  vm = (makeSystem [ testingModule ]).vm;
}
