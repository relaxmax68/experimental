# TODO: find a better way than the extraModules argument.
extraModules:

{config, pkgs, ...}: {
  imports = [ ./hardware-configuration.nix ] ++ extraModules;

  networking.firewall.allowedTCPPorts = [
    # TODO: only expose the journald http gateway port on a VPN interface
    19531 # The journald http gateway port
  ];
  services.journald.enableHttpGateway = true;

  services.openssh = {
      enable = true;
      passwordAuthentication = false;
  };

  environment.systemPackages = with pkgs; [
    smartmontools tmux htop emacs26-nox
  ];

  users.extraUsers.lewo = {
    isNormalUser = true;
    home = "/home/lewo";
    description = "Antoine Eiche";
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBQglK0rutCsTPE9s4g/HkcIZbNZbUjwhOZQ8jHNqVkkUFsl+V3sbekOYSqGnCfqnWthf+L7885COPGTFP5Yg2f7WGaExH7CD1vlsd9bQ1VPQWFzhCXrysaazcYMW/Aggy3RHjNslNW8UOvFa1ccszDz21U0NWmOivFmbhaU2CrUeal3bLuDiIDGP2FSKdkL23d/eMCo63SEsjuMCvXEYqu5zurUoNjmp/buVx3aYZDQh+xnydeN97/O//KFPISCdzDy6gnXKDvQqOqZEI3CsHNlNEf99sOMwZg4xGMXkJ82oDDxUB8bvYn64YmZsFzJO4ciLNat2bLkUHIJLm2oaZ lewo@none" ];
    # Don't know how this is supposed to work:/ So, ATM, we manually
    # have to set a password on the server
    # passwordFile = "/var/secrets/user-lewo";
  };

  # This service checkout the master branch of the repository
  # containing the configuration of this machine every minute, build
  # this configuration and switch to it.
  systemd.services.autoUpgrade = {
    description = "Auto Upgrade";
    wantedBy = [ "multi-user.target" ];
    unitConfig.X-StopOnRemoval = false;
    # Do it every minute
    startAt = "*-*-* *:*:00";
    path = [ pkgs.git pkgs.gnutar pkgs.gzip pkgs.xz.bin config.nix.package.out ];
    script = ''
      set -euxo pipefail

      # TODO: use workingDir instead
      mkdir -p /var/nixos/
      cd /var/nixos/
      git init -q
      git remote add origin https://framagit.org/lewo/experimental.git 2>/dev/null || true
      git fetch origin
      if [[ $(git rev-parse origin/master) == $(git rev-parse HEAD) ]]; then
          echo "No new commit.";
          exit 0;
      fi
      git pull origin master
      nix-build -A system
      # TODO: do not immediately switch to configuration since it could break the machine
      ./result/bin/switch-to-configuration switch
    '';
  };
}
